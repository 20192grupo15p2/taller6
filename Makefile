all: program clear
program: promedio
	gcc obj/promedio.o -lm -o bin/promedio
promedio: src/promedio.c
	gcc -c src/promedio.c -lm -o obj/promedio.o

.PHONY: clear
clear:
	rm obj/*
